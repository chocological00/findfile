use std::{path::Path, fs::File, io::{self,BufWriter}};

use blake3::Hasher;
use clap::Parser;
use walkdir::WalkDir;

#[derive(Parser)]
struct Args
{
	#[arg(short, long, help = "Hash to search for, BLAKE3")]
	b3_hash: String,
	#[arg(short, long, help = "Path to search in")]
	path: String
}

fn main()
{
	let args = Args::parse();
	let path = Path::new(&args.path);

	let mut num_files = 0;
	let mut num_matches = 0;

	for entry in WalkDir::new(path)
	{
		let entry = if let Ok(e) = entry
		{
			e
		}
		else
		{
			continue;
		};
		if !entry.file_type().is_file()
		{
			continue;
		}
		
		let mut file = if let Ok(f) = File::open(entry.path())
		{
			f
		}
		else
		{
			continue;
		};

		let hasher = Hasher::new();
		let mut writer = BufWriter::with_capacity(16384, hasher);
		let size = io::copy(&mut file, &mut writer).unwrap();
		let hash = writer.into_inner().unwrap().finalize();

		num_files += 1;

		if args.b3_hash.to_ascii_lowercase() == hash.to_hex().to_string()
		{
			let p = entry.path().strip_prefix(path).unwrap().display().to_string();
			println!("Found file: {} ({} bytes)", p, size);
			num_matches += 1;
		}
	}

	println!("Searched {} files, found {} matches", num_files, num_matches);
}
